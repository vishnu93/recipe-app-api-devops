variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "vishnu26121993@gmail.com"
}

variable "db_username" {
  description = "user name for RDS postgress instance"
}

variable "db_password" {
  description = "password for RDS postgress instance"
}
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
variable "ecr_image_api" {
  description = "ecr image for API"
  default     = "001427525166.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}
variable "ecr_image_proxy" {
  description = "ecr image for proxy"
  default     = "001427525166.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"

}
variable "django_secret_key" {
  description = "secret key for django app"
}
variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9fXzMpCOqBM5HbezmZgDKrkPJa5ixb2ob75E48SqlThDsJAJzRMV27c+14tT3Bt9PHeUfV1dfuT+RV9HaQJNOCmJFWSFZ4NvTx+6ceGfdLrEENzjiC6b9u46+4kgN6zpc6/tFV4rq/quaDeEAb6ebBfTppCy8pKKk0Y8vWy8V4smSonq1mZG1muterGsv1/GvLmTNC7QXkEvoOFV3+LMwTEUV+GzrMJ3mlJzlOhTALo7s/TcKOj+VJLhZ2nUEO0aQ5cctWVfCaimUsQER3hv1cWtm7eq9jm2906GNI5tqHz0rYiD1V6QYv+LNP1lMEPBeUGNduqTlcWih4NjsdVGf vishnu.vardhan@ET-C02T91MYG8WN.local"
}
variable "dns_zone_name" {
  description = "custom dns for recipe app"
  default     = "recipeappdevops.net"
}

variable "sub_domain" {
  description = "sub domain for custom dns"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
