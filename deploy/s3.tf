resource "aws_s3_bucket" "app_public_files" {
  bucket        = "files-${local.prefix}"
  acl           = "public-read"
  force_destroy = true
}